import React, { useState } from "react";
import { DataFilterContext } from "../../contexts/DataFilterContext";
import ControlPanel from "../ControlPanel/ControlPanel";
import { TableNameContext } from "../../contexts/TableNameContext";
import VariablesTable from "../VariablesTable/VariablesTable";
import { VariablesContext } from "../../contexts/VariablesContext";
import FilteredDataTable from "../FilteredDataTable/FilteredDataTable";
import { DataLoadingContext } from "../../contexts/DataLoadingContext";
import { Spin } from "antd";
import AlgorithmDisplay from "../AlgorithmDisplay/AlgorithmDisplay";
import { Form, Formik } from "formik";
import { TSNE_PARAMS, UMAP_PARAMS } from "../../common/constants";

/**
 * Creates MainDisplay component out of partial components
 * and returns JSX element.
 * @returns MainDisplay React component.
 */
function MainDisplay() {
  // Maintain state for contexts used throughout the app
  const [filteredData, setFilteredData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [tableName, setTableName] = useState(null);
  const [variables, setVariables] = useState([]);

  return (
    <TableNameContext.Provider value={{ tableName, setTableName }}>
      <DataFilterContext.Provider value={{ filteredData, setFilteredData }}>
        <ControlPanel />
        {tableName && (
          <VariablesContext.Provider value={{ variables, setVariables }}>
            <DataLoadingContext.Provider value={{ loading, setLoading }}>
              <VariablesTable />
              {loading ? (
                <div style={{ marginTop: "0.5rem", textAlign: "center" }}>
                  <Spin tip="Calculating data..." size="large" />
                </div>
              ) : (
                <FilteredDataTable />
              )}
              {!loading && (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    gap: "0.5rem",
                    justifyContent: "center",
                  }}
                >
                  <Formik>
                    <Form>
                      <AlgorithmDisplay methodType="PCA" />
                    </Form>
                  </Formik>
                  <Formik
                    initialValues={{
                      perplexity: 50,
                      epsilon: 5,
                    }}
                  >
                    <Form>
                      <AlgorithmDisplay
                        methodType="TSNE"
                        params={TSNE_PARAMS}
                      />
                    </Form>
                  </Formik>
                  <Formik
                    initialValues={{
                      n_neighbors: 10,
                      local_connectivity: 1,
                      min_dist: 1,
                    }}
                  >
                    <Form>
                      <AlgorithmDisplay
                        methodType="UMAP"
                        params={UMAP_PARAMS}
                      />
                    </Form>
                  </Formik>
                </div>
              )}
            </DataLoadingContext.Provider>
          </VariablesContext.Provider>
        )}
      </DataFilterContext.Provider>
    </TableNameContext.Provider>
  );
}

export default MainDisplay;
