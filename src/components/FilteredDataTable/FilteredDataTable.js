import React from "react";
import { Table } from "antd";
import { useFilteredDataTableColumns } from "./hooks/useFitleredDataTableColumns";
import { useDataFilterContext } from "../../contexts/DataFilterContext";

/**
 * Creates a FilteredDataTable with option to run the DR algorithms
 * with the current value of filtered data.
 * @returns FilteredDataTable React component.
 */
function FilteredDataTable() {
  const columns = useFilteredDataTableColumns();
  const { filteredData } = useDataFilterContext();

  return (
    <>
      <Table
        columns={columns}
        dataSource={filteredData}
        style={{ margin: "0.5rem", marginBottom: 0 }}
        pagination={false}
        scroll={{ x: 6000, y: 300 }}
      />
    </>
  );
}

export default FilteredDataTable;
