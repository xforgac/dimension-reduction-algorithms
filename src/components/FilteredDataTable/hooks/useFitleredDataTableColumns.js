/**
 * A hook to build and retrieve FilteredDataTable columns.
 * @returns A list of columns for FilteredDataTable.
 */
export const useFilteredDataTableColumns = () => {
  let columns = [
    {
      fixed: "left",
      render: (_text, _record, index) => <a>{`VAR${index + 1}`}</a>,
    },
  ];

  // Each dataset contains 50 run instances (samples)
  for (let i = 0; i < 50; i++) {
    columns.push({
      title: `S${i + 1}`,
      dataIndex: `sample_${i}`,
      key: `sample_${i}`,
    });
  }

  return columns;
};
