import React from "react";
import { Select, Space, Button, Row } from "antd";
import { DATASETS } from "../../common/constants";
import { useTableNameContext } from "../../contexts/TableNameContext";
import { useLoggedUserContext } from "../../contexts/LoggedUserContext";
import { LogoutOutlined } from "@ant-design/icons";

const { Option } = Select;

/**
 * Creates and returns ControlPanel component which
 * handles dataset selection and logout.
 * @returns ControlPanel React component.
 */
function ControlPanel() {
  const { app, user, setUser } = useLoggedUserContext();
  const { tableName, setTableName } = useTableNameContext();

  const handleLogout = () => {
    user.logOut();
    app.deleteUser(user);
    setUser(null);
  };

  return (
    <Row justify="center" style={{ paddingTop: "0.5rem" }}>
      <Space>
        <Select value={tableName} onChange={(value) => setTableName(value)}>
          <Option value={null}>Dataset</Option>
          {DATASETS.map((dataset, index) => (
            <Option key={`table-option-${index}`} value={dataset.value}>
              {dataset.displayName}
            </Option>
          ))}
        </Select>
        <Button
          type="danger"
          icon={<LogoutOutlined />}
          onClick={handleLogout}
          title={"Logout from MongoDB realm"}
        >
          Logout
        </Button>
      </Space>
    </Row>
  );
}

export default ControlPanel;
