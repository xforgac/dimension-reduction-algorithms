import React, { useEffect, useState } from "react";
import { Table, Button, Space } from "antd";
import { useVariableTableColumns } from "./hooks/useVariableTableColumns";
import { PlusSquareOutlined, SearchOutlined } from "@ant-design/icons";
import { useVariablesContext } from "../../contexts/VariablesContext";
import { useLoggedUserContext } from "../../contexts/LoggedUserContext";
import { useTableNameContext } from "../../contexts/TableNameContext";
import { useDataFilterContext } from "../../contexts/DataFilterContext";
import { reduceVariableProps } from "./utils";
import { uniqWith, isEqual } from "lodash";
import { deepCopy } from "../../common/utils";
import { useDataLoadingContext } from "../../contexts/DataLoadingContext";

/**
 * Creates a VariablesTable with control buttons to add new variables and
 * remove existing ones.
 * @returns VariablesTable React component.
 */
function VariablesTable() {
  const columns = useVariableTableColumns();

  const [containsDuplicates, setContainsDuplicates] = useState(false);
  const [variablesAreEmpty, setVariablesAreEmpty] = useState(true);
  const [newKey, setNewKey] = useState(0);

  const { setFilteredData } = useDataFilterContext();
  const { setLoading } = useDataLoadingContext();
  const { user } = useLoggedUserContext();
  const { tableName } = useTableNameContext();
  const { variables, setVariables } = useVariablesContext();

  const handleVariableAdd = () => {
    // Variable is blank when first added
    const newVariable = {
      key: newKey,
      response: null,
      action: null,
      hops_count: null,
      messages_count: null,
      dst_ip: "",
      dst_ip_id: null,
      dst_service: null,
    };

    setNewKey(newKey + 1);
    setVariables([...variables, newVariable]);
  };

  const handleFilter = async () => {
    let filteredData = [];
    for (let i = 0; i < variables.length; i++) {
      // Filters copy of the data and doesn't change the orignal data
      let variable = deepCopy(variables[i]);

      let row = { key: i };
      reduceVariableProps(variable);

      // Calculate results for all 50 instances (samples)
      setLoading(true);
      for (let j = 0; j < 50; j++) {
        // Query to MongoDB returns count of result records
        const resultCount = await user
          ?.mongoClient("mongodb-atlas")
          .db("messages")
          .collection(`${tableName}_${j}`)
          .count(variable);

        row[`sample_${j}`] = resultCount;
      }
      filteredData.push(row);
    }
    setLoading(false);
    setFilteredData(filteredData);
  };

  useEffect(() => {
    setVariablesAreEmpty(variables.length === 0);

    // Don't edit variables directly
    const variablesCopy = deepCopy(variables);

    /* Delete key because it is unique
       for each variable and would ruin comparison */
    variablesCopy.forEach((variable) => {
      delete variable.key;
    });
    setContainsDuplicates(
      variablesCopy.length !== uniqWith(variablesCopy, isEqual).length
    );
  }, [variables]);

  return (
    <>
      <Table
        columns={columns}
        dataSource={variables}
        style={{ margin: "0.5rem" }}
        pagination={false}
        scroll={{ y: 300 }}
      />
      <Space>
        <Button
          type="primary"
          icon={<PlusSquareOutlined />}
          style={{ marginLeft: "0.5rem " }}
          onClick={handleVariableAdd}
        >
          Add a variable
        </Button>
        <Button
          disabled={variablesAreEmpty || containsDuplicates}
          title={
            variablesAreEmpty
              ? "Create at least one variable!"
              : containsDuplicates
              ? "Variables must be unique!"
              : ""
          }
          type="primary"
          onClick={handleFilter}
          icon={<SearchOutlined />}
        >
          Calculate input data
        </Button>
      </Space>
    </>
  );
}

export default VariablesTable;
