import {
  ACTIONS,
  DESTINATION_IDS,
  DESTINATION_SERVICES,
  NUMERIC_INPUTS,
  RESPONSES,
  TEXT_INPUTS,
} from "../../../common/constants";
import { Input, InputNumber, Select, Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import { useVariablesContext } from "../../../contexts/VariablesContext";
import { deepCopy } from "../../../common/utils";

const { Option } = Select;

/**
 * A hook to build and retrieve VariablesTable columns.
 * @returns A list of columns for VariablesTable.
 */
export const useVariableTableColumns = () => {
  const { variables, setVariables } = useVariablesContext();

  const handleDelete = (key) => {
    // Do not edit variables directly
    let newVariables = deepCopy(variables);
    newVariables = newVariables.filter((variable) => variable.key !== key);
    setVariables(newVariables);
  };

  const handleEdit = (key, fieldName, value) => {
    // Do not edit variables directly
    let newVariables = deepCopy(variables);
    newVariables = newVariables.map((variable) => {
      if (variable.key === key) {
        variable[fieldName] = value;
      }
      return variable;
    });
    setVariables(newVariables);
  };

  let columns = [
    {
      render: (_text, _record, index) => <a>{`VAR${index + 1}`}</a>,
    },
  ];

  TEXT_INPUTS.forEach((input, index) => {
    columns.push({
      title: input.displayName,
      dataIndex: input.name,
      key: input.name,
      render: (_text, record) => (
        <Input
          key={`text-input-${index}`}
          onChange={(event) =>
            handleEdit(record.key, input.name, event.target.value)
          }
          placeholder={input.displayName}
        />
      ),
    });
  });

  columns.push({
    title: "Destination ID",
    dataIndex: "dst_ip_id",
    key: "dst_ip_id",
    render: (_text, record) => {
      return (
        <Select
          style={{ width: "100%" }}
          onChange={(value) => handleEdit(record.key, "dst_ip_id", value)}
          placeholder="Destination ID"
        >
          <Option value={null}>Destination ID</Option>
          {DESTINATION_IDS.map((type, index) => (
            <Option key={`destination-id-option-${index}`} value={type}>
              {type}
            </Option>
          ))}
        </Select>
      );
    },
  });

  columns.push({
    title: "Destination service",
    dataIndex: "dst_service",
    key: "dst_service",
    render: (_text, record) => (
      <Select
        style={{ width: "100%" }}
        onChange={(value) => handleEdit(record.key, "dst_service", value)}
        placeholder="Destination service"
      >
        <Option value={null}>Destination service</Option>
        {DESTINATION_SERVICES.map((type, index) => (
          <Option key={`destination-service-option-${index}`} value={type}>
            {type}
          </Option>
        ))}
      </Select>
    ),
  });

  columns.push({
    title: "Action",
    dataIndex: "action",
    key: "action",
    render: (_text, record) => (
      <Select
        style={{ width: "100%" }}
        onChange={(value) => handleEdit(record.key, "action", value)}
        placeholder="Action"
      >
        <Option value={null}>Action</Option>
        {ACTIONS.map((type, index) => (
          <Option key={`action-option-${index}`} value={type}>
            {type}
          </Option>
        ))}
      </Select>
    ),
  });

  columns.push({
    title: "Result",
    dataIndex: "response",
    key: "response",
    render: (_text, record) => (
      <Select
        style={{ width: "100%" }}
        onChange={(value) => handleEdit(record.key, "response", value)}
        placeholder="Result"
      >
        <Option value={null}>Result</Option>
        {RESPONSES.map((type, index) => (
          <Option key={`response-option-${index}`} value={type}>
            {type}
          </Option>
        ))}
      </Select>
    ),
  });

  NUMERIC_INPUTS.forEach((input, index) => {
    columns.push({
      title: input.displayName,
      dataIndex: input.name,
      key: input.name,
      render: (_text, record) => (
        <InputNumber
          key={`numeric-input-${index}`}
          placeholder={input.displayName}
          controls={false}
          style={{ width: "100%" }}
          onChange={(value) => handleEdit(record.key, input.name, value)}
        />
      ),
    });
  });

  columns.push({
    key: "delete",
    render: (_text, record) => (
      <Button
        type="danger"
        onClick={() => handleDelete(record.key)}
        style={{ width: "100%" }}
        icon={<DeleteOutlined />}
      >
        Delete
      </Button>
    ),
  });

  return columns;
};
