import { isEmpty, isNil, isNumber } from "lodash";

/**
 * Take a variable object as a parameter and reduce it's props
 * to be suited for query input.
 * @param {{
 *  key: number,
 *  name: string,
 *  dst_ip: string,
 *  dst_ip_id: string | null,
 *  dst_service: string | null,
 *  action: string | null,
 *  response: string | null,
 *  hops_count: number | null,
 *  messages_count: number | null,
 * }} variable
 */
export const reduceVariableProps = (variable) => {
  delete variable.key;

  if (isEmpty(variable.dst_ip)) {
    delete variable.dst_ip;
  }
  if (isNil(variable.dst_ip_id)) {
    delete variable.dst_ip_id;
  }
  if (isNil(variable.dst_service)) {
    delete variable.dst_service;
  }
  if (isNil(variable.action)) {
    delete variable.action;
  }
  if (isNil(variable.response)) {
    delete variable.response;
  }
  if (!isNumber(variable.hops_count)) {
    delete variable.hops_count;
  }
  if (!isNumber(variable.messages_count)) {
    delete variable.messages_count;
  }
};
