import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { useProjectionContext } from "../../../contexts/ProjectionContext";
import { useClustersContext } from "../../../contexts/ClustersContext";

/**
 * Creates a resulting scatterplot from the DR projection.
 * @returns Scatterplot React component.
 */
function Scatterplot() {
  const canvas = useRef(null);
  const { projection } = useProjectionContext();
  const { clusters } = useClustersContext();

  // Render scatterplot after each projection change
  useEffect(() => {
    renderCanvas();
  }, [projection]);

  // Setup the scatterplot size
  const width = 800;
  const height = 500;

  // Setup shape and color generation
  const color = d3.scaleOrdinal(d3.schemeDark2).domain(Array.from(new Set(clusters)));
  const shape = d3.scaleOrdinal(
    d3.symbols.map((s) => d3.symbol().size(50).type(s)())
  );

  // Setup scale from frame size
  const get_scales = (data) => {
    let x_extent = d3.extent(data, (d) => d[0]);
    let y_extent = d3.extent(data, (d) => d[1]);
    let x_span = x_extent[1] - x_extent[0];
    let y_span = y_extent[1] - y_extent[0];
    const offset = Math.abs(x_span - y_span) / 2;

    if (x_span > y_span) {
      x_extent[0] -= offset;
      x_extent[1] += offset;
    } else {
      y_extent[0] -= offset;
      y_extent[1] += offset;
    }

    return [
      d3
        .scaleLinear()
        .domain(x_extent)
        .rangeRound([20, width - 20])
        .nice(),
      d3
        .scaleLinear()
        .domain(y_extent)
        .rangeRound([20, height - 20])
        .nice(),
    ];
  };

  const renderCanvas = async () => {
    // Remove previous scatterplot
    let svg = d3.select(canvas.current).selectChildren().remove();

    // Setup empty frame
    svg = d3
      .select(canvas.current)
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewbox", [0, 0, width, height])
      .attr("preserverAspectRatio", "xminYmin")
      .style("background-color", "white")
      .style("border", "2px solid darkgray");

    if (projection) {
      const projectionArray = projection.to2dArray;
      let [x, y] = await new Promise((res) => res(get_scales(projectionArray)));

      // Add tooltip and set it hidden as default
      let tooltip = d3
        .select(canvas.current)
        .append("div")
        .style("position", "absolute")
        .style("opacity", 0)
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "1px")
        .style("border-radius", "5px")
        .style("padding", "5px");

      const mouseover = (event, d) => {
        let sampleNumber = 1;
        for (let i = 0; i < projectionArray.length; i++) {
          if (
            projectionArray[i][0] === d[0] &&
            projectionArray[i][1] === d[1]
          ) {
            sampleNumber = i + 1;
            break;
          }
        }

        tooltip
          .html(`S${sampleNumber}`)
          .style("left", `${event.pageX + 10}px`)
          .style("top", `${event.pageY}px`)
          .style("opacity", 1);
      };

      const mouseleave = () => {
        tooltip.style("opacity", 0);
      };

      // Add points and assign them fill color, opacity and tooltip
      const gs = svg
        .selectAll(".point")
        .data(projectionArray)
        .enter()
        .append("g")
        .attr("class", "point")
        .attr("fill", (_, i) => color(clusters[i]))
        .attr("opacity", 0.75)
        .on("mouseover", mouseover)
        .on("mouseleave", mouseleave);

      // Assign each point a shape (circle)
      gs.append("path").attr("d", () => shape());

      // Scale the points
      svg
        .selectAll(".point")
        .data(projectionArray)
        .attr("transform", ([px, py]) => `translate(${x(px)}, ${y(py)})`);
    }
  };

  return <div ref={canvas} style={{ textAlign: "center" }} />;
}

export default Scatterplot;
