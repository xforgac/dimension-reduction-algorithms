import { InputNumber, Row, Space, Button } from "antd";
import { useFormikContext } from "formik";
import React from "react";
import { useDataFilterContext } from "../../../contexts/DataFilterContext";
import { PlayCircleOutlined } from "@ant-design/icons";
import { isNil } from "lodash";
import { useProjectionContext } from "../../../contexts/ProjectionContext";
import * as druid from "@saehrimnir/druidjs/dist/druid.esm";
import { deepCopy } from "../../../common/utils";
import * as d3 from "d3";
import { useClustersContext } from "../../../contexts/ClustersContext";

/**
 * Creates a paramter panel for DR algorithm.
 * @param {{
 *  methodType: string,
 *  params: string[],
 * }} Props
 * @returns ParametersPanel React component.
 */
function ParametersPanel({ methodType, params }) {
  const { values, setFieldValue } = useFormikContext();
  const { filteredData } = useDataFilterContext();
  const { setClusters } = useClustersContext();
  const { setProjection } = useProjectionContext();

  const incompleteData = filteredData.length < 2;

  // Check for missing parameters
  const objectValues = Object.values(values ?? {});
  const incompleteParameters =
    objectValues.length !==
    objectValues.filter((value) => !isNil(value)).length;

  // Remove key from the data items
  const getDataArray = () => {
    const dataArray = filteredData.map((item) => {
      let newItem = deepCopy(item);
      delete newItem.key;
      return Object.values(newItem);
    });
    return dataArray;
  };

  // Calculate and set clusters of the original high dimensional data
  const calculateClusters = () => {
    if (!incompleteData) {
      const dataArray = getDataArray();
      const dataMatrix = druid.Matrix.from(dataArray).T;
      const distanceMatrix = druid.distance_matrix(dataMatrix, druid.euclidean);
      const H = new druid.Hierarchical_Clustering(distanceMatrix, "complete", "precomputed");

      const nodes = H.root.descendants();
      const threshold = d3.max(nodes, node => node["dist"]) * 0.5;
      const HClusters = H.get_clusters(threshold, "distance");

      let clusters = Array.from({ length: dataArray.length });
      for (let clusterIndex = 0; clusterIndex < HClusters.length; ++clusterIndex) {
        HClusters[clusterIndex].forEach(({index}) => clusters[index] = clusterIndex);
      }
      setClusters(clusters);
    }
  };

  // Select method from prop and execute dimensional reduction
  const calculateProjection = () => {
    if (!incompleteData) {
      const dataArray = getDataArray();
      const matrix = druid.Matrix.from(dataArray).T;
      const method = druid[methodType];

      let projection = new method(matrix);
      Object.keys(values ?? {}).forEach((key) => {
        projection = projection.parameter(key, values[key]);
      });
      projection = projection.transform();

      setProjection(projection);
    }
  };

  return (
    <Row
      justify="center"
      style={{ paddingTop: "0.5rem", paddingBottom: "0.5rem" }}
    >
      <Space>
        {params.map((param, index) => {
          // Padding of 4 to fit in for whitespaces around prefix and input
          const inputWidth = `${
            (values?.[param.name] ?? param.defaultValue).toString().length +
            param.name.length +
            4
          }ch`;

          return (
            <InputNumber
              key={`parameter-input-${index}`}
              name={param.name}
              controls={false}
              placeholder={param.defaultValue}
              onChange={(value) => setFieldValue(param.name, value)}
              value={values?.[param.name]}
              style={{ width: inputWidth }}
              prefix={<b>{param.name}:</b>}
              title={param.description ?? ""}
            />
          );
        })}
        <Button
          disabled={incompleteData || incompleteParameters}
          title={
            incompleteData
              ? "Input data must contain at least two variables!"
              : incompleteParameters
              ? "All paramteres need to be filled!"
              : "Run DR algorithm and apply hierarchical clustering."
          }
          type="primary"
          icon={<PlayCircleOutlined />}
          onClick={() => { 
            calculateClusters();
            calculateProjection(); 
          }}
        >
          {`Run ${methodType}`}
        </Button>
      </Space>
    </Row>
  );
}

export default ParametersPanel;
