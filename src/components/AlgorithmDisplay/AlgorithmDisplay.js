import React, { useState } from "react";
import { ClustersContext } from "../../contexts/ClustersContext";
import { ProjectionContext } from "../../contexts/ProjectionContext";
import ParametersPanel from "./components/ParametersPanel";
import Scatterplot from "./components/Scatterplot";

function AlgorithmDisplay({ methodType = "PCA", params = [] }) {
  const [clusters, setClusters] = useState(null);
  const [projection, setProjection] = useState(null);

  return (
    <ClustersContext.Provider value={{ clusters, setClusters }}>
      <ProjectionContext.Provider value={{ projection, setProjection }}>
        <ParametersPanel methodType={methodType} params={params} />
        <Scatterplot />
      </ProjectionContext.Provider>
    </ClustersContext.Provider>
  );
}

export default AlgorithmDisplay;
