import { noop } from "lodash";
import { createContext, useContext } from "react";

export const TableNameContext = createContext({
  tableName: null,
  setTableName: (_user = null) => noop(),
});

/**
 * A hook to retrieve and maintain the state of currently selected dataset.
 * @returns Current TableNameContext value.
 */
export const useTableNameContext = () => useContext(TableNameContext);
