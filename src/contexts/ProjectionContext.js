import { noop } from "lodash";
import { createContext, useContext } from "react";

export const ProjectionContext = createContext({
  projection: null,
  setProjection: (_projection = null) => noop(),
});

/**
 * A hook to retrieve and maintain the state of a projection produced by a DR algorithm.
 * @returns Current ProjectionContext value.
 */
export const useProjectionContext = () => useContext(ProjectionContext);
