import { noop } from "lodash";
import { createContext, useContext } from "react";

export const ClustersContext = createContext({
  clusters: null,
  setClusters: (_projection = null) => noop(),
});

/**
 * A hook to retrieve and maintain the state of clusters calculated from original data.
 * @returns Current ClustersContext value.
 */
export const useClustersContext = () => useContext(ClustersContext);
