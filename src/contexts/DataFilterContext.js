import { noop } from "lodash";
import { createContext, useContext } from "react";

export const DataFilterContext = createContext({
  filteredData: [],
  setFilteredData: (_data = []) => noop(),
});

/**
 * A hook to retrieve and maintain the state of filtered data.
 * @returns Current DataFilterContext value.
 */
export const useDataFilterContext = () => useContext(DataFilterContext);
