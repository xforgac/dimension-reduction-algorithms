import { noop } from "lodash";
import { createContext, useContext } from "react";

export const VariablesContext = createContext({
  variables: [],
  setVariables: (_data = []) => noop(),
});

/**
 * A hook to retrieve and maintain the state of variables used to filter data.
 * @returns Current VariablesContext value.
 */
export const useVariablesContext = () => useContext(VariablesContext);
