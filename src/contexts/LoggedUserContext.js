import { noop } from "lodash";
import { createContext, useContext } from "react";

export const LoggedUserContext = createContext({
  app: null,
  user: null,
  setUser: (_user = null) => noop(),
});

/**
 * A hook to retrieve and maintain the state of current user and running app.
 * @returns Current LoggedUserContext value.
 */
export const useLoggedUserContext = () => useContext(LoggedUserContext);
