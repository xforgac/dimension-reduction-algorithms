import { noop } from "lodash";
import { createContext, useContext } from "react";

export const DataLoadingContext = createContext({
  loading: false,
  setLoading: (_loading = false) => noop(),
});

/**
 * A hook to retrieve and maintain loading state.
 * @returns Current DataLoadingContext value.
 */
export const useDataLoadingContext = () => useContext(DataLoadingContext);
