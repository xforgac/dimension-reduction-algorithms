import "./App.css";
import MainDisplay from "./components/MainDisplay/MainDisplay";
import "antd/dist/antd.css";
import * as Realm from "realm-web";
import { useState } from "react";
import { Button, Row } from "antd";
import { LoggedUserContext } from "./contexts/LoggedUserContext";
import { LoginOutlined } from "@ant-design/icons";

// Create a MongoDB realm application instance.
const REALM_APP_ID = "cyst-messages-realm-xihes";
const app = new Realm.App({ id: REALM_APP_ID });

/**
 * Enables anonymous login into MongoDB realm.
 * Takes a setter function as a parameter, creates a user instance, logs in
 * and sets this user for the rest of the App to access.
 * @param {{ setUser: React.Dispatch<any> }} setUser
 * @returns Login React component
 */
function Login({ setUser }) {
  const loginAnonymous = async () => {
    const user = await app.logIn(Realm.Credentials.anonymous());
    setUser(user);
  };
  return (
    <>
      <Row justify="center" style={{ paddingTop: "0.5rem" }}>
        <Button
          type="primary"
          icon={<LoginOutlined />}
          onClick={loginAnonymous}
          title={"Login to MongoDB realm as anonymous user"}
        >
          Login
        </Button>
      </Row>
    </>
  );
}

function App() {
  const [user, setUser] = useState(null);

  return (
    <>
      <LoggedUserContext.Provider value={{ app, user, setUser }}>
        {user ? <MainDisplay /> : <Login setUser={setUser} />}
      </LoggedUserContext.Provider>
    </>
  );
}

export default App;
