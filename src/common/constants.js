export const DATASETS = Object.freeze([
  { displayName: "cto", value: "cto_scenario_modular" },
  { displayName: "employee", value: "employee_scenario_modular" },
  { displayName: "vpn", value: "vpn_scenario_modular" },
]);

export const TEXT_INPUTS = Object.freeze([
  { name: "dst_ip", displayName: "Destination IP" },
]);

export const NUMERIC_INPUTS = Object.freeze([
  { name: "hops_count", displayName: "Total hops" },
  { name: "messages_count", displayName: "Total messages" },
]);

export const RESPONSES = Object.freeze([
  "NODE|SUCCESS",
  "NODE|ERROR",
  "NETWORK|SUCCESS",
  "NETWORK|FAILURE",
  "SERVICE|SUCCESS",
  "SERVICE|FAILURE",
]);

export const ACTIONS = Object.freeze([
  "rit:active_recon:host_discovery",
  "rit:active_recon:information_discovery",
  "rit:active_recon:service_discovery",
  "rit:active_recon:vulnerability_discovery",
  "rit:ensure_access:command_and_control",
  "rit:destroy:data_destruction",
  "rit:disclosure:data_exfiltration",
  "rit:privilege_escalation:root_privilege_escalation",
  "rit:targeted_exploits:exploit_remote_services",
]);

export const DESTINATION_SERVICES = Object.freeze([
  "bash",
  "iis",
  "mssql",
  "postfix",
  "powershell",
  "rdp",
  "skysea_client_view",
  "windows_server_2019",
]);

export const DESTINATION_IDS = Object.freeze([
  "api_srv",
  "cto_pc",
  "db_srv",
  "dc_srv",
  "email_srv",
  "emp_pc",
  "infrastructure_router",
  "partner_pc",
  "perimeter_router",
  "vpn_srv",
  "web_srv",
]);

export const TSNE_PARAMS = Object.freeze([
  {
    name: "perplexity",
    defaultValue: 50,
    description:
      "Loosely defines how to balance attention between local\nand global aspects of the data. The parameter is, in a sense,\na guess about the number of close neighbors each point has.\nValue is typically between 5 and 50.",
  },
  {
    name: "epsilon",
    defaultValue: 5,
    description:
      "Learning rate of the algorithm over its\niterations, often called epsilon.",
  },
]);

export const UMAP_PARAMS = Object.freeze([
  {
    name: "n_neighbors",
    defaultValue: 10,
    description:
      "Controls how UMAP balances local versus global structure in\nthe data. It does this by constraining the size of the local\nneighborhood UMAP will look at when attempting to learn the\nmanifold structure of the data.",
  },
  {
    name: "local_connectivity",
    defaultValue: 1,
    description:
      "Ensures that we focus on the difference in distances among nearest\nneighbors rather than the absolute distance (which shows little differentiation\namong neighbors).",
  },
  {
    name: "min_dist",
    defaultValue: 1,
    description:
      "Controls how tightly UMAP is allowed to pack points together.\nIt, quite literally, provides the minimum distance apart that\npoints are allowed to be in the low dimensional representation.",
  },
]);
