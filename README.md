# Dimension reduction algorithms

A simple application which lets you setup multiple input variables and various filters for them. Filters are applied into query that fetches the count of result data and slots them into table. The result table is used as input into 3 DR algorithms - PCA, UMAP and t-SNE - producing scatterplots used for visual analaysis.

## Prerequisites

* [Node.js v17.7.2](https://nodejs.org/en)
* Yarn v1.22.17 (see Installation section for more info)

## Installation

* Install Yarn using `npm install --global yarn`
* If you want to use `v1.22.17` set it for the project with `yarn set version <version>`
* Install project dependencies using `yarn install`
* You can now run the application with `yarn start`
* Happy visualising!

## Backend and database

There is no backend present directly in this project. Every query is executed using [MongoDB](https://www.mongodb.com/). Upon opening the application you get asked to login with a button. This logs you into MongoDB realm as an anonymous user and lets you make read requests to the database.

Data is split into 150 different tables - each representing a single instance (sample S#) of an attack in a given configuration. There are 3 different configurations each containing 50 samples:
* cto
* employee
* vpn

## Usage

You can select your configuration after logging in. Selecting configuration will unlock a new table where you can setup variables (VAR#) and assign them different filters for the data. Upon filtering the variables a query is executed which calculates count of results corresponding to each filter on each of the samples in the selected configuration.

This table is the input that is passed into DR algorithms which create scatterplot visualisation for visual analysis. You can run DR algorithms by a button under the table to display the resulting scatterplots.

## Authors and acknowledgment
Coded and developed by Filip Forgáč. Consulted and advised by Vít Rusňák.

## License
Copyright 2022, Filip Forgáč

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
